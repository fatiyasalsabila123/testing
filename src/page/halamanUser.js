import React, { useEffect, useState } from "react";
import axios from "axios";
import { Button, Pagination, Table } from "react-bootstrap";
import "bootstrap/dist/css/bootstrap.min.css";

function HalamanUser() {
  const [users, setUsers] = useState([]);
  const [currentPage, setCurrentPage] = useState(1);
  useEffect(() => {
    // Mengambil data dari API menggunakan Axios
    axios
      .get("https://fakestoreapi.com/users")
      .then((response) => {
        setUsers(response.data);
      })
      .catch((error) => {
        console.error("Error fetching data:", error);
      });
  }, []);

  

  return (
    <div style={{ padding: "20px" }}>
      <h1>List of Users</h1>
      <Table striped bordered hover>
        <thead>
          <tr>
            <th>Firs Name</th>
            <th>Email</th>
            <th>Username</th>
            <th>Phone</th>
            <th>Address</th>
            <th>Latitude</th>
            <th>Actiom</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user) => (
            <tr key={user.id}>
              <td>
                {user.name.firstname} {user.name.lastname}
              </td>
              <td>{user.email}</td>
              <td> {user.username}</td>
              <td>{user.phone}</td>
              <td>
                {user.address.street}, {user.address.city},{" "}
                {user.address.zipcode}
              </td>
              <td>
                {" "}
                {user.address.geolocation.lat}, Longitude:{" "}
                {user.address.geolocation.long}
              </td>
              <td>
                <Button>Edit</Button>
              </td>
            </tr>
          ))}
        </tbody>
      </Table>
    </div>
  );
}

export default HalamanUser;
