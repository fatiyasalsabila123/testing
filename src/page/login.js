// import { useState } from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom";
import "../style/login.css";
import React, { useState } from "react";

function Login() {
  const [username, setUsername] = useState("");
  const [password, setPassword] = useState("");
  const [message, setMessage] = useState("");
  const history = useHistory();

  const handleLogin = () => {
    // Kirim permintaan login ke API palsu
    fetch("https://fakestoreapi.com/auth/login", {
      method: "POST",
      body: JSON.stringify({
        username:"mor_2314",
        password:"83r5^_",
       }),
      headers: {
        "Content-Type": "application/json",
      },
    })
      .then((res) => res.json())
      .then((json) => {
        if (json.success) {
          setMessage("Login berhasil");
          history.push("/halamanUser");
          setTimeout(() => {
            window.location.reload();
          }, 1500);
        } else {
          setMessage("Login gagal. Periksa kembali username dan password.");
        }
      })
      .catch((error) => {
        setMessage("Terjadi kesalahan saat melakukan login.");
        console.error(error);
      });
  };
  return (
    <div style={{background:"black"}}>
      <div class="background">
        <div class="shape"></div>
        <div class="shape"></div>
      </div>
      <form onSubmit={handleLogin}>
        <h3>Login Here</h3>

        <label for="username">username</label>
        <input
          type="text"
          placeholder="username"
          id="username"
          value={username}
          onChange={(e) => setUsername(e.target.value)} required
        />

        <label for="password">Password</label>
        <input
          type="password"
          placeholder="Password"
          id="password"
          value={password}
          onChange={(e) => setPassword(e.target.value)} required
        />

        <button type="submit">Log In</button>
        <p>{message}</p>
      </form>
    </div>
  );
}

export default Login;
