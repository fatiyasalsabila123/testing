import { HashRouter, Routes, Route } from "react-router-dom";
import Login from "./page/login";
import { BrowserRouter } from "react-router-dom/cjs/react-router-dom.min";
import HalamanUser from "./page/halamanUser";

function App() {
  return (
    <BrowserRouter>
      <Route path="/" component={Login} exact />
      <Route path="/halamanUser" component={HalamanUser} exact />
    </BrowserRouter>
  );
}

export default App;
